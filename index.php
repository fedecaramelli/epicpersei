<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
  <title>Federico Caramelli</title>

  <!-- Bootstrap -->
  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <link rel='shortcut icon' type='image/x-icon' href='favicon.ico' />

  <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Architects+Daughter" rel="stylesheet">

  <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->

      <link rel="stylesheet" href="css/buttons.css" >
      <link rel="stylesheet" href="css/fede.css" >
      <link rel="stylesheet" href="css/mediafede.css" >
      

    </head>
    <body>
      <div id="bottomBar">
        <div class="container-fluid">
          <div id="icon-holders">
            <div onclick="openUrl('https://www.linkedin.com/in/federico-caramelli-a7598a27')" class="social-icon linkedin" alt="linkedIn"></div>
            <div class="icon-divider"></div>
            <div onclick="openUrl('https://open.spotify.com/user/1267856898')" class="social-icon spotify"></div>
            <div onclick="openUrl('https://soundcloud.com/fede-tri-tri')" class="social-icon soundcloud"></div>
            <div class="icon-divider"></div>
            <div onclick="openUrl('https://github.com/seedwalk')" class="social-icon github"></div>
            <div onclick="openUrl('https://bitbucket.org/fedecaramelli/')" class="social-icon bitbucket"></div>
            <div class="icon-divider"></div>
            <div onclick="toSection('contacto')" class="social-icon mail"></div>
          </div>
        </div>
      </div>
      <section class="zone" id="portada">

        <div class="container portada-holder">

          <img class="mifoto" src="img/foto.png">
          <h1>Federico Caramelli</h1>
          <h2>Full Stack Developer</h2>
        </div>


      </section>

      <section class="zone" id="aboutme">

        <div class="container aboutme-holder"> 
          <h1>About Me!</h1>

          <p>Music lover, guitarist, coder, self-learner, gamer, libra, indigo. jedi, traveler. Who the cap fit let them wear it. </p>

        </div>


      </section>
      <div class="instaGramFeed" style="height: 520px;">
        <?php 
        include('instagram.php'); 
        ?>
      </div>
      <section class="zone" id="skills">

        <div class="container skills-holder"> 
          <h1>My Skills!</h1>
          <div class="sign three">
           <img src="http://www.clker.com/cliparts/G/G/T/i/X/u/british-crown-md.png">

           <p>HTML5<br/> CSS3<br/>JavaScript<br/>JQuery<br/>MySQL<br/>PHP<br/>REST APIs<br/>Wordpress</li>
           </p>
         </div>


       </div>

     </section>



     <section class="zone" id="works">
      <div class="facility orange slideDown" id="green_haus_restaurant">
        <div class="facility_img stretchMe" rel="thumbnails/carrera.jpg" title="Confiteria Carrera"></div>
        <div class="facility_info">
        <div class="facility_title">Carrera<span class="facility_close">X</span></div>
          <div class="facility_desc">
            <p>HTML layout, frontend, e-commerce, implementation of the payment gateway.</p>
            <a href="http://www.carrera.com.uy" target="_blank">Link</a>
          </div>
          <div class="facility_arrow"></div>
        </div>
      </div>

      <div class="facility red slideDown" id="monsoon_lounge">
        <div class="facility_img stretchMe" rel="thumbnails/lugloc.jpg" title="LugLoc"></div>
        <div class="facility_info">
          <div class="facility_title">LugLoc<span class="facility_close">X</span></div>
          <div class="facility_desc">
            <p>HTML layout, implementation and  integracion to SOAP Api.</p>
            <a href="http://lugloc.com" target="_blank">Link</a>
          </div>
          <div class="facility_arrow"></div>
        </div>
      </div>

      <div class="facility yellow slideDown" id="gymnasium">
        <div class="facility_img stretchMe" rel="thumbnails/cloudlance.jpg" title="Cloudlance"></div>
        <div class="facility_info">
          <div class="facility_title">Cloudlance<span class="facility_close">X</span></div>
          <div class="facility_desc long">
            <p>HTML layout, CMS, web app for freelancers.</p>
            <a href="http://cloudlance.co" target="_blank">Link</a>
          </div>
          <div class="facility_arrow"></div>
        </div>
      </div>

      <div class="facility blue slideLeft" id="poolside_bar_cafe">
        <div class="facility_img stretchMe" rel="thumbnails/mosk.jpg" title="Mosk"></div>
        <div class="facility_info">
          <div class="facility_title">Mosk<span class="facility_close">X</span></div>
          <div class="facility_desc">
            <p>HTML layout, frontend coding and CMS Development.</p>
            <a href="http://mosk.com.uy" target="_blank">Link</a>
          </div>
          <div class="facility_arrow"></div>
        </div>
      </div>

      <div class="facility orange slideDown" id="silver_leaf">
        <div class="facility_img stretchMe" rel="thumbnails/tanooki.jpg" title="Tanooki"></div>
        <div class="facility_info">
          <div class="facility_title">Tanooki<span class="facility_close">X</span></div>
          <div class="facility_desc">
            <p>CMS, HTML Layout y fontend coding.</p>
            <a href="http://tanooki.com.uy" target="_blank">Link</a>
          </div>
          <div class="facility_arrow"></div>
        </div>
      </div>

      <div class="facility red slideRight" id="club_lounge">
        <div class="facility_img stretchMe" rel="thumbnails/yama.jpg" title="Yamagroup"></div>
        <div class="facility_info">
          <div class="facility_title">Yamagroup<span class="facility_close">X</span></div>
          <div class="facility_desc">
            <p>HTML layout, fontend coding, CMS.</p>
          </div>
          <div class="facility_arrow"></div>
        </div>
      </div>

     

    </section>



    <section class="zone" id="contacto">
     <div class="container contacto-holder"> 
      <h1>Mail me!</h1>
      <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 col-xs-offset-3">
          <form id="contact-form" class="form" action="#" method="POST" role="form">
            <div class="form-group">
              <label class="form-label" for="name">Your Name</label>
              <input type="text" class="form-control" id="name" name="name" placeholder="Your name" tabindex="1" required>
            </div>                            
            <div class="form-group">
              <label class="form-label" for="email">Your Email</label>
              <input type="email" class="form-control" id="email" name="email" placeholder="Your Email" tabindex="2" required>
            </div>                            
            <div class="form-group">
              <label class="form-label" for="subject">Subject</label>
              <input type="text" class="form-control" id="subject" name="subject" placeholder="Subject" tabindex="3">
            </div>                            
            <div class="form-group">
              <label class="form-label" for="message">Message</label>
              <textarea rows="5" cols="50" name="message" class="form-control" id="message" placeholder="Message..." tabindex="4" required></textarea>                                 
            </div>
            <div class="text-center">

              <button class="button button--saqui button--inverted button--text-thick button--text-upper button--size-s" data-text="Send Message">Send Message</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>


  <div id="resolution">

    Screen resolution too small :'(
    
  </div>




  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  <script src="js/backstretch.js"> </script>
  <script src="js/fede.js"> </script>



</body>
</html>